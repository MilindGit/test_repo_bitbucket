cleartool mktrtype -c "Trigger to avoid rmelem" -element -all -preop rmelem -execwin "ccperl -e \"exit 1\"" NO_RMELEM

cleartool mktrtype -c "Trigger to avoid rmname" -element -all -preop rmname -execwin "ccperl -e \"exit 1\"" NO_RM_NAME

cleartool lstype -kind trtype -invob \AIRS_CVOB
cleartool lstype -l -kind trtype -invob \AIRS_CVOB

cleartool rmtype trtype:"NO_RM_NAME"